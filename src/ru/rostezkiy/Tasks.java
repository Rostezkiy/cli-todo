package ru.rostezkiy;

import java.util.ArrayList;
import java.util.Scanner;

public class Tasks {
    final String ANSI_RED = "\u001B[31m";
    final String ANSI_RESET = "\u001B[0m";
    ArrayList<String> myTasks = new ArrayList<>();

    public Tasks() {
    }

    public void addTask(String taskDescription) {
        myTasks.add(taskDescription);
    }

    public void size() {
        System.out.println(myTasks.size() + " element(s) in the list: ");

        int i = 1;
        for (String myTask : myTasks) {

            System.out.println(i++ + ") " + myTask);
        }
    }

    public void delete(int part) {
        if (part > myTasks.size() | part<1) {
            System.out.printf(ANSI_RED + "Error." + ANSI_RESET + "Input correct number.");
        } else {
            myTasks.remove(part - 1);
            System.out.println("Task " + part + " deleted.");
        }
    }

    public void clearList() {
        boolean exit = false;


        System.out.println(ANSI_RED + "That will delete all existing tasks. Are you sure? " + ANSI_RESET + " y / n ");
        while (!exit) {
            Scanner userChoice = new Scanner(System.in);
            char c = userChoice.next().charAt(0);
            if (c == 'y') {
                myTasks.clear();
                System.out.println("List cleared");
                myTasks.size();
                exit = true;
            } else if (c == 'n') {
                break;
            } else System.out.println("Input 'y' or 'n' \n");
        }
    }
}