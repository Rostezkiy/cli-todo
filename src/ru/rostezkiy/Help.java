package ru.rostezkiy;

public class Help {
    public static void run() {
        System.out.println("Available commands: \n" +
                " add - add new task \n" +
                " del - delete existing task (enter number of the task) \n" +
                " show - view task list \n" +
                " clear - clear all tasks \n" +
                " help - open that screen \n" +
                " exit - close application");
    }
}
