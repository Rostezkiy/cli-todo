package ru.rostezkiy;

import java.util.Scanner;

public class ToDo {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static void main(String[] args) {
        Tasks myTasks = new Tasks();
        Scanner userInput = new Scanner(System.in);
        boolean exit = false;
        while (!exit) {
            System.out.println("\n Enter command: ");
            String command = userInput.nextLine();
            String[] parts = command.split(" ", 2);
            switch (parts[0]) {
                case "add":
                    if (parts.length == 1) {
                        System.out.println(ANSI_RED + "Error. " + ANSI_RESET + "Input task description.");
                    } else {
                        myTasks.addTask(parts[1]);
                        System.out.println("Task " + parts[1] + " added, your task list is: ");
                        myTasks.size();
                    }
                    break;
                case "show":
                    myTasks.size();
                    break;
                case "del":
                    try {
                        Integer.parseInt(parts[1]);
                    } catch (NumberFormatException e) {
                        System.out.println(ANSI_RED + "Error. " + ANSI_RESET + "Input number");
                        break;
                    }
                    if (parts.length == 1) {
                        System.out.println(ANSI_RED + "Error. " + ANSI_RESET + "Input task number.");
                    } else {
                        myTasks.delete(Integer.parseInt(parts[1]));
                    }

                    break;
                case "help":
                    Help.run();
                    break;
                case "clear":
                    myTasks.clearList();
                    break;
                case "exit":
                    System.out.println("Good bye!");
                    exit = true;
                    break;
                default:
                    System.out.println("Error occurred. Type 'help' for available commands");
            }
        }
    }
}
