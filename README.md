**Simple CLI ToDo application**

***
Available commands:

	add - add new task (enter task description)
	del - delete existing task (enter number of the task that you want to delete)
	show - view task list 
	clear - clear all tasks 
	help - open help screen with available commands 
	exit - close application

Example input: 

 	add wash dishes
 	add do homework
 	del 2 
 	show
	
Output:

 1) wash dishes